# Copyright (C) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/test.gni")
import("//foundation/distributeddatamgr/data_share/datashare.gni")

group("unittest") {
  testonly = true
  deps = []

  deps += [
    ":ErrorCodeTest",
    ":JoinTest",
    ":NativeDataShareTest",
    ":ProxyDatasTest",
    ":SlientAccessTest",
    "resource/datashare_ext_bundle:datashare_ext",
    "resource/datashareproxy_bundle/proxydatas_with_permission:proxydatas_with_permission",
    "resource/errorcode_ext_bundle:errorcode_ext",
    "resource/ohos_test:copy_ohos_test",
  ]
}

ohos_unittest("NativeDataShareTest") {
  module_out_path = "data_share/native_datashare"

  include_dirs = [
    "//foundation/ability/ability_runtime/interfaces/inner_api/ability_manager/include",
    "//foundation/ability/ability_runtime/interfaces/inner_api/app_manager/include/appmgr",
    "//foundation/ability/ability_runtime/interfaces/inner_api/dataobs_manager/include",
    "//foundation/ability/ability_runtime/interfaces/kits/native/ability/native",
    "./unittest/mediadatashare_test/include",
    "//base/security/access_token/frameworks/common/include",
    "//base/hiviewdfx/hilog/interfaces/native/innerkits/include",
    "//foundation/aafwk/standard/interfaces/innerkits/uri/include",
    "//commonlibrary/c_utils/base/include",
    "//utils/system/safwk/native/include",
    "//foundation/communication/ipc/interfaces/innerkits/ipc_core/include",
    "//third_party/json/include",
  ]

  sources =
      [ "./unittest/mediadatashare_test/src/mediadatashare_unit_test.cpp" ]

  deps = [
    "${datashare_innerapi_path}:datashare_consumer",
    "${datashare_innerapi_path}/common:datashare_common",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_manager",
    "ability_runtime:abilitykit_native",
    "ability_runtime:dataobs_manager",
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libtoken_setproc",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "ipc:ipc_single",
    "media_library:media_library",
    "relational_store:rdb_data_ability_adapter",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("SlientAccessTest") {
  module_out_path = "data_share/native_datashare"

  include_dirs = [
    "//base/security/access_token/frameworks/common/include",
    "//base/hiviewdfx/hilog/interfaces/native/innerkits/include",
    "//foundation/aafwk/standard/interfaces/innerkits/uri/include",
    "//commonlibrary/c_utils/base/include",
    "//utils/system/safwk/native/include",
    "//foundation/communication/ipc/interfaces/innerkits/ipc_core/include",
  ]

  sources = [ "./unittest/mediadatashare_test/src/slientaccess_test.cpp" ]

  deps = [
    "${datashare_innerapi_path}:datashare_consumer",
    "${datashare_innerapi_path}/common:datashare_common",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_manager",
    "ability_runtime:abilitykit_native",
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libtoken_setproc",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "hilog:libhilog",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("ErrorCodeTest") {
  module_out_path = "data_share/native_datashare"

  include_dirs = [
    "//base/security/access_token/frameworks/common/include",
    "//base/hiviewdfx/hilog/interfaces/native/innerkits/include",
    "//foundation/aafwk/standard/interfaces/innerkits/uri/include",
    "//commonlibrary/c_utils/base/include",
    "//utils/system/safwk/native/include",
    "//foundation/communication/ipc/interfaces/innerkits/ipc_core/include",
  ]

  sources = [ "./unittest/mediadatashare_test/src/errorcode_test.cpp" ]

  deps = [
    "${datashare_innerapi_path}:datashare_consumer",
    "${datashare_innerapi_path}/common:datashare_common",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_manager",
    "ability_runtime:abilitykit_native",
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libtoken_setproc",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "hilog:libhilog",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("JoinTest") {
  module_out_path = "data_share/native_datashare"

  include_dirs = [
    "//base/security/access_token/frameworks/common/include",
    "//base/hiviewdfx/hilog/interfaces/native/innerkits/include",
    "//foundation/aafwk/standard/interfaces/innerkits/uri/include",
    "//commonlibrary/c_utils/base/include",
    "//utils/system/safwk/native/include",
    "//foundation/communication/ipc/interfaces/innerkits/ipc_core/include",
  ]

  sources = [ "./unittest/mediadatashare_test/src/join_test.cpp" ]

  deps = [
    "${datashare_innerapi_path}:datashare_consumer",
    "${datashare_innerapi_path}/common:datashare_common",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_manager",
    "ability_runtime:abilitykit_native",
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libtoken_setproc",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "hilog:libhilog",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("ProxyDatasTest") {
  module_out_path = "data_share/native_datashare"

  include_dirs = [
    "//base/security/access_token/frameworks/common/include",
    "//base/hiviewdfx/hilog/interfaces/native/innerkits/include",
    "//foundation/aafwk/standard/interfaces/innerkits/uri/include",
    "//commonlibrary/c_utils/base/include",
    "//utils/system/safwk/native/include",
    "//foundation/communication/ipc/interfaces/innerkits/ipc_core/include",
  ]

  sources =
      [ "./unittest/datashareproxy_test/proxydatas_with_permission_test.cpp" ]

  deps = [
    "${datashare_innerapi_path}:datashare_consumer",
    "${datashare_innerapi_path}/common:datashare_common",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_manager",
    "ability_runtime:abilitykit_native",
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libtoken_setproc",
    "c_utils:utils",
    "hilog:libhilog",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}
